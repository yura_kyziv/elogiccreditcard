<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Block\Adminhtml\System\Config\Form;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Element\Template;

class LiqPayPathConfig extends Field
{

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element): string
    {
        $html = $this->_layout
            ->createBlock(Template::class)
            ->setTemplate('Mastering_ElogicCreditCard::system/config/liqpay_path.phtml')
            ->setCacheable(false)
            ->toHtml();

        return '<div id="row_' . $element->getHtmlId() . '">' . $html . '</div>';
    }
}
