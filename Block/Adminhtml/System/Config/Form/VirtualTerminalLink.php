<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Block\Adminhtml\System\Config\Form;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Element\Template;

class VirtualTerminalLink extends Field
{

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element): string
    {
        $html = $this->_layout
            ->createBlock(Template::class)
            ->setTemplate('Mastering_ElogicCreditCard::system/config/virtual_terminal.phtml')
            ->setCacheable(false)
            ->toHtml();

        return '<div id="row_' . $element->getHtmlId() . '">' . $html . '</div>';
    }
}
