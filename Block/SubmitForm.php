<?php
declare(strict_types=1);

namespace  Mastering\ElogicCreditCard\Block;

use Exception;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\Order;
use Mastering\ElogicCreditCard\Sdk\LiqPay;
use Mastering\ElogicCreditCard\Helper\Redirect\Data as Helper;


class SubmitForm extends Template
{
    protected $order = null;

    /**
     * @var LiqPay
     */
    protected LiqPay $liqPay;


    /**
     * @var Helper
     */
    protected Helper $helper;

    /**
     * @param Template\Context $context
     * @param LiqPay $liqPay
     * @param Helper $helper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        LiqPay $liqPay,
        Helper $helper,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->liqPay = $liqPay;
        $this->helper = $helper;
    }

    /**
     * @return mixed|null
     * @throws Exception
     */
    public function getOrder()
    {
        if ($this->order === null) {
            throw new Exception('Order is not set');
        }
        return $this->order;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return false
     */
    protected function _loadCache(): bool
    {
        return false;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function toHtml(): string
    {
        $order = $this->getOrder();
        $html = $this->liqPay->cnb_form(array(
            'action' => 'pay',
            'amount' => $order->getGrandTotal(),
            'currency' => $order->getOrderCurrencyCode(),
            'description' => $this->helper->getLiqPayDescription($order),
            'order_id' => $order->getIncrementId(),
        ));
        return $html;
    }
}
