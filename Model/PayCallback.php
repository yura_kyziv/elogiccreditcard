<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Model;

use Exception;
use Mastering\ElogicCreditCard\Api\PayCallbackInterface;
use Magento\Sales\Model\Order;
use LiqpayMagento\LiqPay\Sdk\LiqPay;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction;
use Mastering\ElogicCreditCard\Helper\Redirect\Data as Helper;
use Magento\Framework\App\RequestInterface;


class PayCallback implements PayCallbackInterface
{
    /**
     * @var Order
     */
    protected Order $order;

    /**
     * @var LiqPay
     */
    protected LiqPay $liqPay;

    /**
     * @var OrderRepositoryInterface
     */
    protected OrderRepositoryInterface $orderRepository;

    /**
     * @var InvoiceService
     */
    protected InvoiceService $invoiceService;

    /**
     * @var Transaction
     */
    protected Transaction $transaction;

    /**
     * @var Helper
     */
    protected Helper $helper;

    /**
     * @var RequestInterface
     */
    protected RequestInterface $request;

    /**
     * @param Order $order
     * @param OrderRepositoryInterface $orderRepository
     * @param InvoiceService $invoiceService
     * @param Transaction $transaction
     * @param Helper $helper
     * @param LiqPay $liqPay
     * @param RequestInterface $request
     */
    public function __construct(
        Order $order,
        OrderRepositoryInterface $orderRepository,
        InvoiceService $invoiceService,
        Transaction $transaction,
        Helper $helper,
        LiqPay $liqPay,
        RequestInterface $request
    )
    {
        $this->order = $order;
        $this->liqPay = $liqPay;
        $this->orderRepository = $orderRepository;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
        $this->helper = $helper;
        $this->request = $request;
    }

    /**
     * @return null
     */
    public function callback()
    {
        $post = $this->request->getParams();
        if (!(isset($post['data']) && isset($post['signature']))) {
            $this->helper->getLogger()->error(__('In the response from LiqPay server there are no POST parameters "data" and "signature"'));
            return null;
        }

        $data = $post['data'];
        $receivedSignature = $post['signature'];

        $decodedData = $this->liqPay->getDecodedData($data);
        $orderId = $decodedData['order_id'] ?? null;
        $receivedPublicKey = $decodedData['public_key'] ?? null;
        $status = $decodedData['status'] ?? null;
        $amount = $decodedData['amount'] ?? null;
        $currency = $decodedData['currency'] ?? null;
        $transactionId = $decodedData['transaction_id'] ?? null;
        $senderPhone = $decodedData['sender_phone'] ?? null;

        try {
            $order = $this->getRealOrder($status, $orderId);
            if (!($order && $order->getId() && $this->helper->checkOrderIsLiqPayPayment($order))) {
                return null;
            }

        // ALWAYS CHECK signature field from Liqpay server!!!!
        // DON'T delete this block, be careful of fraud!!!
            if (!$this->helper->securityOrderCheck($data, $receivedPublicKey, $receivedSignature)) {
                $order->addStatusHistoryComment(__('LiqPay security check failed!'));
                $this->orderRepository->save($order);
                return null;
            }

            $historyMessage = [];
            $state = null;
            switch ($status) {
                case LiqPay::STATUS_SANDBOX:
                case LiqPay::STATUS_WAIT_COMPENSATION:
                // case LiqPay::STATUS_SUBSCRIBED:
                case LiqPay::STATUS_SUCCESS:
                    if ($order->canInvoice()) {
                        $invoice = $this->invoiceService->prepareInvoice($order);
                        $invoice->register()->pay();
                        $transactionSave = $this->transaction->addObject(
                            $invoice
                        )->addObject(
                            $invoice->getOrder()
                        );
                        $transactionSave->save();
                        if ($status == LiqPay::STATUS_SANDBOX) {
                            $historyMessage[] = __('Invoice #%1 created (sandbox).', $invoice->getIncrementId());
                        } else {
                            $historyMessage[] = __('Invoice #%1 created.', $invoice->getIncrementId());
                        }
                        $state = Order::STATE_PROCESSING;
                    } else {
                        $historyMessage[] = __('Error during creation of invoice.');
                    }
                    if ($senderPhone) {
                        $historyMessage[] = __('Sender phone: %1.', $senderPhone);
                    }
                    if ($amount) {
                        $historyMessage[] = __('Amount: %1.', $amount);
                    }
                    if ($currency) {
                        $historyMessage[] = __('Currency: %1.', $currency);
                    }
                    break;
                case LiqPay::STATUS_FAILURE:
                    $state = Order::STATE_CANCELED;
                    $historyMessage[] = __('Liqpay error.');
                    break;
                case LiqPay::STATUS_ERROR:
                    $state = Order::STATE_CANCELED;
                    $historyMessage[] = __('Liqpay error.');
                    break;
                case LiqPay::STATUS_WAIT_SECURE:
                    $state = Order::STATE_PROCESSING;
                    $historyMessage[] = __('Waiting for verification from the Liqpay side.');
                    break;
                case LiqPay::STATUS_WAIT_ACCEPT:
                    $state = Order::STATE_PROCESSING;
                    $historyMessage[] = __('Waiting for accepting from the buyer side.');
                    break;
                case LiqPay::STATUS_WAIT_CARD:
                    $state = Order::STATE_PROCESSING;
                    $historyMessage[] = __('Waiting for setting refund card number into your Liqpay shop.');
                    break;
                default:
                    $historyMessage[] = __('Unexpected status from LiqPay server: %1', $status);
                    break;
            }
            if ($transactionId) {
                $historyMessage[] = __('LiqPay transaction id %1.', $transactionId);
            }
            if (count($historyMessage)) {
                $order->addStatusHistoryComment(implode(' ', $historyMessage))
                    ->setIsCustomerNotified(true);
            }
            if ($state) {
                $order->setState($state);
                $order->setStatus($state);
                $order->save();
            }
            $this->orderRepository->save($order);
        } catch (Exception $e) {
            $this->helper->getLogger()->critical($e);
        }
        return null;
    }

    /**
     * @param $status
     * @param $orderId
     * @return Order
     */
    protected function getRealOrder($status, $orderId): Order
    {
        if ($status == LiqPay::STATUS_SANDBOX) {
            if (!empty($testOrderSurfix)) {
                $testOrderSurfix = LiqPay::TEST_MODE_SURFIX_DELIM;
                if (strlen($testOrderSurfix) < strlen($orderId)
                    && substr($orderId, -strlen($testOrderSurfix)) == $testOrderSurfix
                ) {
                    $orderId = substr($orderId, 0, strlen($orderId) - strlen($testOrderSurfix));
                }
            }
        }
        return $this->order->loadByIncrementId($orderId);
    }
}