<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Model;

use Mastering\ElogicCreditCard\Api\Data\PaymentMethodCodeInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class RedirectPaymentMethod extends AbstractMethod
{
    protected $_code = PaymentMethodCodeInterface::REDIRECT_CODE;

    protected $_canAuthorize = 'true';
}