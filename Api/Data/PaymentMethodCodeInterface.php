<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Api\Data;

/**
 * Interface PaymentMethodCodeInterface
 */
interface PaymentMethodCodeInterface
{
    const CODE = 'elogic_credit_card';

    const REDIRECT_CODE = 'elogic_credit_card_redirect';
}
