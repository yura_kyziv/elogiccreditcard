<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Api\Data;

/**
 * Interface EnvironmentStatusInterface
 * @api
 */
interface EnvironmentStatusInterface
{
    const PRODUCTION = 'production';
    const SANDBOX = 'sandbox';
}
