<?php

namespace Mastering\ElogicCreditCard\Api;

/**
 * Interface ResponseFieldsInterface
 */
interface ResponseFieldsInterface
{
    const ACQUIRER_ID = 'acq_id';
    const ACTION = 'action';
    const PAYMENT_ID = 'payment_id';
    const VERSION = 'version';
    const PAY_TYPE = 'paytype';
    const ORDER_ID = 'order_id';
    const LIQPAY_ORDER_ID = 'liqpay_order_id';
    const TRANSACTION_ID = 'transaction_id';
    const END_DATE = 'end_date';
    const SENDER_CARD_MASK = 'sender_card_mask2';
    const SENDER_CARD_BANK = 'sender_card_bank';
    const SENDER_CARD_TYPE = 'sender_card_type';
    const STATUS = 'status';
}
