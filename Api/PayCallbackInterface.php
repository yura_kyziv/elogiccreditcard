<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Api;


interface PayCallbackInterface
{
    /**
     * @api
     * @return null
     */
    public function callback();
}