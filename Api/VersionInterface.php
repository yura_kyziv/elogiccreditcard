<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Api;

/**
 * Interface VersionInterface
 * @api
 */
interface VersionInterface
{
    /**
     * @var int
     */
    const VERSION = 3;
}
