<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Api;

/**
 * Interface ApiUrlInterface
 * @api
 */
interface ApiUrlInterface
{
    const API_URL = 'https://www.liqpay.ua/api/';
    const REQUEST_ENDPOINT_PATH = 'request';
}
