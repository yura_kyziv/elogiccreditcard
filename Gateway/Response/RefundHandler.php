<?php

declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Response;

use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Model\InfoInterface;
use Mastering\ElogicCreditCard\Api\ResponseFieldsInterface;

/**
 * Class PaymentHandler
 */
class RefundHandler implements HandlerInterface
{
    /**
     * @var array
     */
    private array $additionalInformation = [
        ResponseFieldsInterface::ACTION,
        ResponseFieldsInterface::PAYMENT_ID,
        ResponseFieldsInterface::STATUS,
    ];

    /**
     * @param array $handlingSubject
     * @param array $response
     * @return void
     */
    public function handle(array $handlingSubject, array $response): void
    {
        /** @var InfoInterface $payment */
        $payment = $handlingSubject['payment']->getPayment();

        foreach ($this->additionalInformation as $responseKey) {
            if (!empty($response[$responseKey])) {
                $payment->setAdditionalInformation($responseKey, $response[$responseKey]);
            }
        }
    }
}
