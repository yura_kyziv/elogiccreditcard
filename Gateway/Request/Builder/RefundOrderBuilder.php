<?php

declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Request\Builder;

use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Mastering\ElogicCreditCard\Gateway\Request\OrderIdProvider;
use Mastering\ElogicCreditCard\Api\RequestFieldsInterface as RequestFields;

/**
 * Class RefundOrderBuilder
 */
class RefundOrderBuilder implements BuilderInterface
{

    /**
     * @var OrderIdProvider
     */
    private OrderIdProvider $orderIdProvider;

    /**
     * RefundOrderBuilder constructor.
     * @param OrderIdProvider $orderIdProvider
     */
    public function __construct(OrderIdProvider $orderIdProvider)
    {
        $this->orderIdProvider = $orderIdProvider;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject): array
    {
        /** @var OrderAdapterInterface $order */
        $order = $buildSubject['payment']->getOrder();

        return [
            RequestFields::AMOUNT => $buildSubject['amount'],
            RequestFields::ORDER_ID => $this->orderIdProvider->get($order),
        ];
    }
}
