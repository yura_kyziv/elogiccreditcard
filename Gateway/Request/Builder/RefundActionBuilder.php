<?php

declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Request\Builder;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Mastering\ElogicCreditCard\Api\Data\PaymentActionInterface;
use Mastering\ElogicCreditCard\Api\RequestFieldsInterface as RequestFields;

/**
 * Class RefundActionBuilder
 */
class RefundActionBuilder implements BuilderInterface
{

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject): array
    {
        return [
            RequestFields::ACTION => PaymentActionInterface::REFUND,
        ];
    }
}
