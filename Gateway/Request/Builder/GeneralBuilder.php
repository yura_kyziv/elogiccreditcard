<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Request\Builder;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Mastering\ElogicCreditCard\Api\RequestFieldsInterface as RequestFields;
use Mastering\ElogicCreditCard\Api\VersionInterface;
use Mastering\ElogicCreditCard\Gateway\Config;

/**
 * Class GeneralBuilder
 */
class GeneralBuilder implements BuilderInterface
{
    /**
     * @var Config
     */
    private Config $config;


    /**
     * GeneralBuilder constructor.
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject): array
    {
        return [
            RequestFields::VERSION => VersionInterface::VERSION,
            RequestFields::PUBLIC_KEY => $this->config->getPublicKey(),
        ];
    }
}
