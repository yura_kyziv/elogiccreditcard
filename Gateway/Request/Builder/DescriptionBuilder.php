<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Request\Builder;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Mastering\ElogicCreditCard\Api\RequestFieldsInterface as RequestFields;

/**
 * Class DescriptionBuilder
 */
class DescriptionBuilder implements BuilderInterface
{
    /**
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject): array
    {
        return [
            RequestFields::DESCRIPTION => 'LiqPay for Magento 2',
        ];
    }
}
