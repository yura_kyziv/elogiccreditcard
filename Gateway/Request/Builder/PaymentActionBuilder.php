<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Request\Builder;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Mastering\ElogicCreditCard\Gateway\Request\PaymentActionProvider;
use Mastering\ElogicCreditCard\Api\RequestFieldsInterface as RequestFields;

/**
 * Class PaymentActionBuilder
 */
class PaymentActionBuilder implements BuilderInterface
{
    /**
     * @var PaymentActionProvider
     */
    private PaymentActionProvider $actionProvider;

    /**
     * PaymentActionBuilder constructor.
     * @param PaymentActionProvider $actionProvider
     */
    public function __construct(PaymentActionProvider $actionProvider)
    {
        $this->actionProvider = $actionProvider;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject): array
    {
        return [
            RequestFields::ACTION => $this->actionProvider->getPaymentAction(),
        ];
    }
}
