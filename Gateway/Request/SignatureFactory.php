<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Request;

use Mastering\ElogicCreditCard\Gateway\Config;

/**
 * Class SignatureFactory
 */
class SignatureFactory
{
    /**
     * @var Encoder
     */
    private Encoder $encoder;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * SignatureFactory constructor.
     * @param Encoder $encoder
     * @param Config $config
     */
    public function __construct(
        Encoder $encoder,
        Config $config
    ) {
        $this->encoder = $encoder;
        $this->config = $config;
    }

    /**
     * @param string $encryptedData
     * @return string
     */
    public function create(string $encryptedData): string
    {
        $privateKey = $this->config->getPrivateKey();
        return $this->encoder->encode(sha1($privateKey . $encryptedData . $privateKey, true));
    }
}
