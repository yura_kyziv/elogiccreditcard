<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Request;

/**
 * To formats payment data according to LiqPay api requirements
 * Class PaymentDataFormatter
 */
class CardPaymentDataFormatter
{
    /**
     * @param string $value
     * @return string
     */
    public function getFormattedYear(string $value): string
    {
        return substr($value, -2);
    }

    /**
     * @param string $value
     * @return string
     */
    public function getFormattedMonth(string $value): string
    {
        return strlen($value) < 2 ? '0' . $value : $value;
    }
}
