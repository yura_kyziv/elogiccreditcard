<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Request;

use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Mastering\ElogicCreditCard\Gateway\Config;

/**
 * Class OrderIdProvider
 */
class OrderIdProvider
{
    /**
     * @var Config
     */
    private Config $config;

    /**
     * OrderIdProvider constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param OrderAdapterInterface $order
     * @return string
     */
    public function get(OrderAdapterInterface $order): string
    {
        return $this->config->getOrderPrefix() . $order->getOrderIncrementId() . $this->config->getOrderSuffix();
    }
}
