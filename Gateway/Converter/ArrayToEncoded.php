<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Converter;

use Magento\Framework\Serialize\Serializer\Json;
use Magento\Payment\Gateway\Http\ConverterInterface;
use Mastering\ElogicCreditCard\Gateway\Request\Encoder;
use Mastering\ElogicCreditCard\Gateway\Request\SignatureFactory;

/**
 * Class ArrayToEncoded
 */
class ArrayToEncoded implements ConverterInterface
{
    /**
     * @var Encoder
     */
    private Encoder $encoder;

    /**
     * @var Json
     */
    private Json $serializer;

    /**
     * @var SignatureFactory
     */
    private SignatureFactory $signatureFactory;

    /**
     * ArrayToEncoded constructor.
     * @param Encoder $encoder
     * @param Json $serializer
     * @param SignatureFactory $signatureFactory
     */
    public function __construct(
        Encoder $encoder,
        Json $serializer,
        SignatureFactory $signatureFactory
    ) {
        $this->encoder = $encoder;
        $this->serializer = $serializer;
        $this->signatureFactory = $signatureFactory;
    }

    /**
     * @param array $response
     * @return array|string
     */
    public function convert($response)
    {
        $data = $response['data'];

        $encryptedData = $this->encoder->encode($this->serializer->serialize($data));

        return http_build_query([
            'data' => $encryptedData,
            'signature' => $this->signatureFactory->create($encryptedData),
        ]);
    }
}
