<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Gateway\Validator;

use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ResultInterface;

/**
 * Class GeneralResponseValidator
 */
class RefundResponseValidator extends AbstractValidator
{
    /**
     * @param array $validationSubject
     * @return ResultInterface
     */
    public function validate(array $validationSubject): ResultInterface
    {
        $response = $validationSubject['response'];

        $isValid = true;
        $errorMessages = [];

        foreach ($this->getResponseValidators() as $validator) {
            $validationResult = $validator($response);

            if (!$validationResult[0]) {
                $isValid = $validationResult[0];
                $this->addErrorMessages($errorMessages, $validationResult);
            }
        }

        return $this->createResult($isValid, $errorMessages);
    }

    /**
     * @param array $errorMessages
     * @param array $validationResult
     */
    private function addErrorMessages(array &$errorMessages, array $validationResult)
    {
        $errorMessages = array_merge($errorMessages, $validationResult[1]);
    }

    /**
     * @return array
     */
    private function getResponseValidators(): array
    {
        return [
            function ($response) {
                return [
                    isset($response['action']),
                    [__('LiqPay Action is missing in the response')]
                ];
            },
            function ($response) {
                return [
                    isset($response['payment_id']),
                    [__('LiqPay Payment Id is missing in the response')]
                ];
            },
            function ($response) {
                return [
                    isset($response['status']) && in_array($response['status'], ['success', 'reserved']),
                    [__('LiqPay server returned an error in the response')]
                ];
            },
        ];
    }
}
