<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Ui\DataProvider;

use Magento\Ui\DataProvider\AbstractDataProvider as ParentDataProvider;

class DataProvider extends ParentDataProvider
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }
    /**
     * @return array
     */
    public function getData(): array
    {
        return [];
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        return null;
    }
}

