<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Plugin\Payment\Block;

use Mastering\ElogicCreditCard\Api\Data\PaymentMethodCodeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Block\Info as BlockInfo;
use Mastering\ElogicCreditCard\Api\ResponseFieldsInterface;

/**
 * Class Info
 */
class Info
{
    /**
     * @var array
     */
    private $labels = [
        ResponseFieldsInterface::SENDER_CARD_MASK => 'Card Number',
        ResponseFieldsInterface::SENDER_CARD_TYPE => 'Type',
    ];

    /**
     * @var array
     */
    private $values = [];

    /**
     * @param BlockInfo $subject
     * @param $result
     * @return array
     * @throws LocalizedException
     */
    public function afterGetSpecificInformation(
        BlockInfo $subject,
        $result
    ): array
    {
        //$subject->getData('info')->getData('method')
        if (PaymentMethodCodeInterface::CODE === $subject->getData('info')->getData('method')) {
            $payment = $subject->getInfo();
            $additionalData = $payment->getAdditionalInformation();
            foreach ($this->labels as $key => $label) {
                if (array_key_exists($key, $additionalData)) {
                    $value = $additionalData[$key];
                    if (isset($this->values[$key][$value])) {
                        $value = $this->values[$key][$value];
                    }
                    $result[$label] = $value;
                }
            }
        }

        return $result;
    }
}
