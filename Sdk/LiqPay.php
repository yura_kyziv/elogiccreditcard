<?php
declare(strict_types = 1);

namespace Mastering\ElogicCreditCard\Sdk;

use Mastering\ElogicCreditCard\Api\VersionInterface;
use Mastering\ElogicCreditCard\Gateway\Config;
use Mastering\ElogicCreditCard\Helper\Redirect\Data;

/** extends official LiqPay Sdk */
class LiqPay extends \LiqPay
{
    const TEST_MODE_SUFFIX_DELIM = '-test';

    // failure
    const STATUS_FAILURE     = 'failure';
    const STATUS_ERROR       = 'error';


    /**
     * @var Config
     */
    protected Config $helper;

    /**
     * @param Config $helper
     * @param Data $redirectHelper
     */
    public function __construct(
        Config $helper,
        Data $redirectHelper
    )
    {
        $this->helper = $helper;
        if ($redirectHelper->isEnabled()) {
            $publicKey = $helper->getPublicKey();
            $privateKey = $helper->getPrivateKey();
            parent::__construct($publicKey, $privateKey);
        }
    }

    /**
     * @param $params
     * @return mixed
     */
    protected function prepareParams($params)
    {
        if (!isset($params['sandbox'])) {
            $params['sandbox'] = (int)$this->helper->isSandbox();
        }
        if (!isset($params['version'])) {
            $params['version'] = VersionInterface::VERSION;
        }
        if (isset($params['order_id']) && $this->helper->isSandbox()) {
            $params['order_id'] .= self::TEST_MODE_SUFFIX_DELIM;
        }
        return $params;
    }

    /**
     * @return Config
     */
    public function getHelper(): Config
    {
        return $this->helper;
    }

    /**
     * @param string $path
     * @param  array $params
     * @param int $timeout
     * @return string
     */
    public function api($path, $params = [], $timeout = 5): string
    {
        $params = $this->prepareParams($params);
        return parent::api($path, $params, $timeout);
    }

    /**
     * @param array $params
     * @return string
     */
    public function cnb_form($params): string
    {
        $params = $this->prepareParams($params);
        return parent::cnb_form($params);
    }
}
