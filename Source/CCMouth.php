<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Source;

use Magento\Framework\Data\OptionSourceInterface;

class CCMouth implements OptionSourceInterface
{

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $result = [];

        $mouths = [
            '01',
            '02',
            '03',
            '04',
            '05',
            '06',
            '07',
            '08',
            '09',
            '10',
            '11',
            '12',
        ];

        foreach ($mouths as $mouth) {
            $result[] = [
                'value' => $mouth,
                'label' => __($mouth)
            ];
        }

        return $result;
    }
}
