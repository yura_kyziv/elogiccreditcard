<?php
declare(strict_types = 1);

namespace Mastering\ElogicCreditCard\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Mastering\ElogicCreditCard\Api\Data\EnvironmentStatusInterface;

/**
 * Class Environment
 */
class Environment implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => EnvironmentStatusInterface::PRODUCTION, 'label' => __('Production')],
            ['value' => EnvironmentStatusInterface::SANDBOX, 'label' => __('Sandbox')],
        ];
    }
}
