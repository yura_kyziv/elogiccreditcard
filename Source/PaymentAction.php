<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Mastering\ElogicCreditCard\Api\Data\PaymentActionInterface;

/**
 * Class PaymentAction
 */
class PaymentAction implements OptionSourceInterface
{
    /**
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => PaymentActionInterface::AUTHORIZE, 'label' => __('Authorize Only')],
            ['value' => PaymentActionInterface::AUTHORIZE_CAPTURE, 'label' => __('Authorize and Capture')],
        ];
    }
}
