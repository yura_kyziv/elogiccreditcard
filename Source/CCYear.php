<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Source;

use Magento\Framework\Data\OptionSourceInterface;

class CCYear implements OptionSourceInterface
{

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $result = [];

        $currentYear = date("Y");
        $currentYear = (int)$currentYear - 2000;


        for ($i = 0; $i < 10; $i++) {
            $result[] = [
                'value' => $currentYear + $i,
                'label' => __($currentYear + $i)
            ];
        }

        return $result;
    }
}
