<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Helper\Redirect;

use Magento\Framework\App\Helper\Context;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Mastering\ElogicCreditCard\Gateway\Config;
use Magento\Sales\Api\Data\OrderInterface;
use Psr\Log\LoggerInterface;
use Mastering\ElogicCreditCard\Model\RedirectPaymentMethod;
use Magento\Framework\Exception\LocalizedException;

class Data extends AbstractHelper
{
    /**#@+
     * Configuration keys
     */
    const REDIRECT_DESCRIPTION = 'payment/elogic_credit_card_redirect/description';
    const REDIRECT_ACTIVE = 'payment/elogic_credit_card_redirect/active';
    /**#@-*/

    /**
     * @var PaymentHelper
     */
    private PaymentHelper $paymentHelper;

    /**
     * @var Config
     */
    private Config $config;

    public function __construct(
        PaymentHelper $paymentHelper,
        Config $config,
        Context $context
    ) {
        $this->paymentHelper = $paymentHelper;
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * @param OrderInterface|null $order
     * @return string
     */
    public function getLiqPayDescription(OrderInterface $order = null): string
    {
        $description = trim($this->scopeConfig->getValue(
            static::REDIRECT_DESCRIPTION,
            ScopeInterface::SCOPE_STORE
        ));
        $params = [
            '{order_id}' => $order->getIncrementId(),
        ];
        return strtr($description, $params);
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        if ($this->scopeConfig->getValue(
            self::REDIRECT_ACTIVE,
            ScopeInterface::SCOPE_STORE
        )
        ) {
            if ($this->config->getPublicKey() && $this->config->getPrivateKey()) {
                return true;
            } else {
                $this->_logger->error(__('The payment module is turned off, because public or private key is not set'));
            }
        }
        return false;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->_logger;
    }

    /**
     * @param OrderInterface $order
     * @return bool
     * @throws LocalizedException
     */
    public function checkOrderIsLiqPayPayment(OrderInterface $order): bool
    {
        $method = $order->getPayment()->getMethod();
        $methodInstance = $this->paymentHelper->getMethodInstance($method);
        return $methodInstance instanceof RedirectPaymentMethod;
    }

    /**
     * @param $data
     * @param $receivedPublicKey
     * @param $receivedSignature
     * @return bool
     */
    public function securityOrderCheck($data, $receivedPublicKey, $receivedSignature): bool
    {
        $publicKey = $this->config->getPublicKey();
        if ($publicKey !== $receivedPublicKey) {
            return false;
        }

        $privateKey = $this->config->getPrivateKey();
        $generatedSignature = base64_encode(sha1($privateKey . $data . $privateKey, false));

        return $receivedSignature === $generatedSignature;
    }
}
