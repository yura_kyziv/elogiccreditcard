<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Helper\Terminal;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**#@+
     * Configuration keys
     */
    const DESCRIPTION = 'payment/elogic_credit_card_terminal/description';
    const CARD_NUMBER = 'payment/elogic_credit_card_redirect/card_number';
    const YEAR = 'payment/elogic_credit_card_redirect/year';
    const MOUTH = 'payment/elogic_credit_card_redirect/mouth';
    const CVN = 'payment/elogic_credit_card_redirect/cvn';
    /**#@-*/

    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    private function getDescription()
    {
        return $this->scopeConfig->getValue(self::DESCRIPTION, ScopeInterface::SCOPE_STORE);
    }

    private function getCardNumber()
    {
        return $this->scopeConfig->getValue(self::CARD_NUMBER, ScopeInterface::SCOPE_STORE);
    }

    private function getYear()
    {
        return $this->scopeConfig->getValue(self::YEAR, ScopeInterface::SCOPE_STORE);
    }

    private function getMouth()
    {
        return $this->scopeConfig->getValue(self::MOUTH, ScopeInterface::SCOPE_STORE);
    }

    private function getCVN()
    {
        return $this->scopeConfig->getValue(self::CVN, ScopeInterface::SCOPE_STORE);
    }
}
