define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list',
    ],
    function (Component, rendererList) {
        'use strict';

        return Component.extend({
            /**
             *  @returns this
             */
            initialize: function () {
                this._super();

                rendererList.push(
                    {
                        type: 'elogic_credit_card',
                        component: 'Mastering_ElogicCreditCard/js/view/payment/method-renderer/cc-form'
                    }
                );

                return this;
            },
        });
    }
);
