define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'elogic_credit_card_redirect',
                component: 'Mastering_ElogicCreditCard/js/view/payment/method-renderer/redirect'
            }
        );
        return Component.extend({});
    }
);