<?php
declare(strict_types=1);

namespace  Mastering\ElogicCreditCard\Controller\Checkout;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\LayoutFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Mastering\ElogicCreditCard\Helper\Redirect\Data as Helper;
use Mastering\ElogicCreditCard\Block\SubmitForm;
use Magento\Framework\Controller\Result\Json;
use Exception;


class Form extends Action
{
    /**
     * @var CheckoutSession
     */
    protected CheckoutSession $_checkoutSession;

    /**
     * @var Helper
     */
    protected Helper $helper;

    /**
     * @var LayoutFactory
     */
    protected LayoutFactory $_layoutFactory;

    /**
     * @param Context $context
     * @param CheckoutSession $checkoutSession
     * @param Helper $helper
     * @param LayoutFactory $layoutFactory
     */
    public function __construct(
        Context $context,
        CheckoutSession $checkoutSession,
        Helper $helper,
        LayoutFactory $layoutFactory
    )
    {
        parent::__construct($context);
        $this->_checkoutSession = $checkoutSession;
        $this->helper = $helper;
        $this->_layoutFactory = $layoutFactory;
    }

    /**
     * @return Json
     */
    public function execute(): Json
    {
        try {
            if (!$this->helper->isEnabled()) {
                throw new Exception(__('Payment is not allow.'));
            }
            $order = $this->getCheckoutSession()->getLastRealOrder();
            if (!($order && $order->getId())) {
                throw new Exception(__('Order not found'));
            }
            if ($this->helper->checkOrderIsLiqPayPayment($order)) {
                /* @var $formBlock SubmitForm */
                $formBlock = $this->_layoutFactory->create()->createBlock('Mastering\ElogicCreditCard\Block\SubmitForm');
                $formBlock->setOrder($order);
                $data = [
                    'status' => 'success',
                    'content' => $formBlock->toHtml(),
                ];
            } else {
                throw new Exception('Order payment method is not a LiqPay payment method');
            }
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong, please try again later'));
            $this->helper->getLogger()->error($e);
            $this->getCheckoutSession()->restoreQuote();
            $data = [
                'status' => 'error',
                'redirect' => $this->_url->getUrl('checkout/cart'),
            ];
        }
        /** @var Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result->setData($data);
        return $result;
    }


    /**
     * @return CheckoutSession
     */
    protected function getCheckoutSession(): CheckoutSession
    {
        return $this->_checkoutSession;
    }
}