<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Controller\Adminhtml\Virtual;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Magento_Sales::create';

    /**
     * @var PageFactory $resultPageFactory
     */
    protected PageFactory $resultPageFactory;

    /**
     * Index constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return Page
     */
    public function execute(): Page
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mastering_ElogicCreditCard::virtual_terminal');
        $resultPage->getConfig()->getTitle()->prepend(__('Elogic Credit Card Virtual Terminal'));

        return $resultPage;
    }
}
