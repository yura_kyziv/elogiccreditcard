<?php
declare(strict_types=1);

namespace Mastering\ElogicCreditCard\Controller\Adminhtml\Virtual;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Redirect;
use LiqPay;
use Magento\Framework\Exception\NoSuchEntityException;
use Mastering\ElogicCreditCard\Gateway\Config;
use Magento\Store\Model\StoreManagerInterface;
use Mastering\ElogicCreditCard\Api\VersionInterface;

class Send extends Action
{
    const ADMIN_RESOURCE = 'Mastering_ElogicCreditCard::virtual_terminal';

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * Save constructor.
     * @param Context $context
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        Config $config,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * @return Redirect
     * @throws NoSuchEntityException
     */
    public function execute(): Redirect
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        /** @var Http $request*/
        $request = $this->getRequest();
        $requestData = $request->getPost()->toArray();
        $cardData = $requestData['card_information'];

        $public_key = $this->config->getPublicKey();
        $private_key = $this->config->getPrivateKey();
        $currency = $this->getBaseCurrencyCode();

        $liqPay = new LiqPay($public_key, $private_key);
        $result = $liqPay->api("request", array(
            'action'         => 'auth',
            'version'        => VersionInterface::VERSION,
            'phone'          => '380950000001',
            'amount'         => $cardData['amount'],
            'currency'       => $currency,
            'description'    => 'Test payment',
            'order_id'       => 'test_order_' . date('c'),
            'card'           => $cardData['cc_number'],
            'card_exp_month' => $cardData['mouth'],
            'card_exp_year'  => $cardData['year'],
            'card_cvv'       => $cardData['cvn']
        ));

        if ($result->result == 'error') {
            $this->messageManager->addErrorMessage(__($result->err_code . ':' .$result->err_description));
        } else if ($result->result == 'ok') {
            $this->messageManager->addSuccessMessage(__('Request is send.'));
        } else {
            $this->messageManager->addSuccessMessage(__('Something went wrong.'));
        }

        $resultRedirect->setPath('elogic_credit_card/virtual/index');
        return $resultRedirect;
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    private function getBaseCurrencyCode(): string
    {
        return $this->storeManager->getStore()->getBaseCurrencyCode();
    }
}
